# -*- coding: utf-8 -*-
"""
Created on Thu Aug 11 09:56:33 2016

@author: okan
"""
import flask, flask.views,json
from flask import request
from werkzeug import secure_filename
from gensim import utils
from docSim import simScoreList
import  os
import logging
logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', 
                    level=logging.INFO)
                  
app = flask.Flask(__name__)

#######################Arayüz Kısmı##########################################
@app.route('/', methods=['GET'])
def index():
    return flask.render_template('index.html', benzerlik="",sorgutext="",openText="")

                                 
@app.route('/', methods=['GET', 'POST'])
def openFile():   
    mainPath=os.path.dirname(__file__) # ana dizin bulunuyor     
    if request.method == 'POST':
        f = request.files['file']
        filePath=os.path.join(mainPath,'SorguDokumanlari',secure_filename(f.filename))
        #Sorgu yapılan Dokuman Kayıt
        f.save(filePath)
        dosyaokuma=open(filePath,'r')
        text=dosyaokuma.read()
        dosyaokuma.close()         
        query=text.lower().decode("utf8")
        sorgutext=query
        doc = {'tokens': utils.simple_preprocess(query)}
        ##docsimden benzer liste nın çekilmesi        
        benzerlik = simScoreList(doc)
        #benzer olan dosyaların okunması
        listeuzunluk=[]
        openText=[]
        benzerlik=benzerlik["sim"]
        ##benzer olan dosya uzunluğu
        a=0
        while a<len(benzerlik):        
          listeuzunluk.append(a)
          a+=1
       ##benzer dosya döngü 
        i=0
        while i<len(listeuzunluk):
            dirNames=benzerlik[i][0].split("_") 
            #benzer dosya
            f=open(os.path.join(mainPath,"news1",dirNames[0],dirNames[1]),'r')
            openText.append(f.read().lower().decode("utf8"))    
            f.close() 
            i=i+1
        return flask.render_template('index.html',benzerlik=benzerlik,
                                     sorgutext=sorgutext,listeuzunluk=listeuzunluk,
                                     openText=(openText))   

                                         
if __name__ == "__main__":    
    app.debug = True
    app.run()