
2011-10-17T11:20:0Z
kultur-sanat
İzmir Büyükşehir Belediyesi Ahmet Adnan Saygun Sanat Merkezi (AASSM), 19 Ekim Çarşamba günü İzmir Devlet Opera Ve Balesi Orkestrası sanatçılarından oluşan "Ephesus Brass" grubunu ağırlamaya hazırlanıyor. Ephesus Brass grubu Ömür Gürlük (trompet), Cemal Tilev (trompet), Cüneyt Deniz (korno), Altuğ Tekin (korno), Çetin Subaşı (trombon), Dündar Banaz (trombon), Kaan Karaburun (kontrabas), Serkan Sun (gitar), Akgün Çavuş (vurmalı çalgılar), Türker Barmanbek (piyano), Ender Ünal (keman) ve solist olarak Tevik Rodos, Polet Yurteri, Şevki Deniz ile Renin Yükseler'den oluşuyor. Konser, saat 20.00'de başlayacak. - İZMİR 10 49 18 0
