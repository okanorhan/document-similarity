
2011-11-14T21:16:0Z
kultur-sanat
Yedi yaşındayken Franz Liszt Müzik Akademisi'nin "Özel Yetenekli Çocuklar Sınıfı"na kabul edilen Macar piyanist Adrienne Hauser, pek çok uluslararası festivalde konser verdi. Tamas Vasary, Zoltan Kocsis, Peter Eötvös gibi şeflerin yönetiminde çalan ünlü piyanist, 19 Kasım 2011, Cumartesi günü Akbank Sanat Piyano Günleri'ndeki konserinde, doğumunun 200. yılı anısına ünlü besteci Liszt'in unutulmaz eserlerini seslendirecek. 6 79 27 0
