
2011-05-30T14:51:0Z
kultur-sanat
Özgün ve derinlikli çalışmalarıyla tanınan sanatçı Nur Ataibiş'in; "İsimsiz IV" başlıklı sergisi 8 Haziran'da Kare Sanat Galerisi'nde açılıyor! 1999 yılından bu yana çok sayıda kişisel ve karma sergiye imza atan sanatçı Nur Ataibiş; "İsimsiz IV" başlıklı 9. kişisel sergisinde kullandığı yuvarlak tuvaller ile insanlığın bir türlü kıramadığı şiddet çemberini vurguluyor. 6 78 28 0
