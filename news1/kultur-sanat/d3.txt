
2011-02-12T14:03:00Z
kultur-sanat
Yalova Belediyesi Kültür Etkinlikleri Kapsamında Halk Eğitim Merkezi Konferans Salonu'nda Sahne Alan 'Aşk Her Yerde' Adlı Tiyatro Oyununun Ekibinin Performansı, Davetlilerce Ayakta Alkışlandı. Senaryosu ve oyuncuların performansıyla ortaya çıkan muhteşem performansı, Yalovalılar uzun süre ayakta alkışladılar. İki bölümlük komedinin sonunda Yalova Belediyesi Kültür İşlerinden Sorumlu Başkan Yardımcısı Hikmet Yavuz, oyuncuları başarılarından dolayı tebrik ederek çiçek ve Yürüyen Köşk maketi hediye etti. Yalova'da 2. Kez bulunduğunu söyleyen Emre Kınay ise; "Daha önce Yalova'ya gelmiştik. Bugün 2.kez geliyoruz ve yine salonu doldurdunuz. İlginizden dolayı teşekkür ederiz" diyerek gecenin finalini yapan isim oldu. 9 40 3 0
