
2011-06-20T12:30:0Z
kultur-sanat
Erbaa Belediyesi Şehir Kütüphanesi, engelli okuyucuların evlerine kitap servisi yapıyor. 2 bin 850 üyesi ve 10 bin kitabı bulunan Erbaa Şehir Kütüphanesi yetkilileri, engelli çocukların daha çok hikaye türü kitapları tercih ettiğini, büyüklerin roman ve bilgiye dayalı kitaplara ilgi gösterdiğini belirttiler. Proje hakkında bilgi veren Erbaa Belediye Başkanı Ahmet Yenihan, "Asıl amacımız, toplumumuzun bu konuya dikkatini çekmek. Engelli kardeşlerimizin de bir birey olduklarını herkes gibi onların da sosyal kültürel ihtiyaçları olduğunu unutmamamız gerekiyor. Engelli vatandaşlarımızın toplumumuza entegre olmalarını sağlamanın bir vatandaşlık görevi olduğunu hatırlatmak istedik."dedi. 1 36 15 0
