
2011-05-05T11:47:0Z
kultur-sanat
A. E. Gredianus'un yazdığı, Can Gürzap'ın çevirdiği ve Murat Aslan'ın yönettiği oyunda, gün boyu ev işleriyle uğraşmaktan çok sıkılan ve babalarıyla yaşayan Simplina ve Sandra'nın başından geçenler anlatılıyor. Dekor ve giysi tasarımını Işınsu Ersan, ışık tasarımını Kemal Gürgün'ün hazırladığı oyunda, Aytaç Özgür, Derya Kara Erk, Bengi Atakan, Alpay Yurtseveneler, Melih Karaman, Çelen Kaytaş Hallı, Burak Özbaykuş, Nagehan Yazıcı, Elif Göksüner, Murat Altan ve Burak Emre Akata rol alıyor. "İki Kova Su" adlı çocuk oyunu yarın ve 7 Mayıs'ta, Gaziantep Devlet Tiyatrosu Onat Kutlar Sahnesinde izlenebilecek. "İki Kova Su" adlı oyun bugün de Kahramanmaraş Necip Fazıl Kısakürek Devlet Tiyatrosu Sahnesinde sergilenecek. - GAZİANTEP 6 71 33 0
