
2011-10-21T16:12:0Z
kultur-sanat
Doğuş Grubu'nun, Cumhurbaşkanlığı Senfoni Orkestrası ana sponsorluğu kapsamında başlayan "Kampüste Senfonik Akşamlar" adlı Anadolu turnesi, Hakkari'nin Çukurca ilçesinde meydana gelen terör saldırıları nedeniyle iptal edildi. Doğuş Holding'den yapılan yazılı açıklamada, "Ülkece yaşadığımız acı olayların neticesinde, 22-28 Ekim tarihleri arasında Kayseri, Sivas, Malatya, Urfa ve Mardin'de gerçekleştirilmesi planlanan Cumhurbaşkanlığı Senfoni Orkestrası (CSO) ile Kampüste Senfonik Akşamlar 2011 Turnesi iptal olmuştur" denildi. - İstanbul 6 28 14 0
