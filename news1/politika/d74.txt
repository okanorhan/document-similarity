
2011-04-06T17:51:0Z
politika
Bahreyn'de hem iktidar hem de muhalefetle görüşen, Katar'daysa Libyalı isyancıların temsilcisiyle bir araya gelen Davutoğlu, bugün de Suriye'nin başkenti Şam'da Devlet Başkanı beşşar Esad tarafından kabul edildi. Görüşmede Davutoğlu, Türkiye'nin, Suriye hükümetinin yaklaşık 3 hafta önce açıkladığı reform paketine desteğini dile getirdi. Beşşar Esad da, gerçekleştirmeyi planladıkları anayasal reformlar konusunda da Türkiye gibi bir ülkenin tecrübelerinden faydalanmaktan memnuniyet duyduklarını ifade etti. Davutoğlu, Esad ile Cumhurbaşkanlığı Sarayı'nda yapılan görüşmenin ardından Suriyeli mevkidaşı Velid Muallim ile bir araya geldi. 7 29 24 0
