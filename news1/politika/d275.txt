
2012-12-16T13:56:0Z
politika
Başbakan Recep Tayyip Erdoğan, toplu açılış törenleri için Konya'ya geldi. Başbakan Erdoğan'ı taşıyan özel uçak ANA, saat 12.55'te Konya Havaalanına indi. Sağlık Bakanı Recep Akdağ, Konya Valisi Aydın Nezih Doğan, Ak Parti İl Başkanı Ahmet Sorgun, Büyükşehir Belediye Başkanı Tahir Akyürek, Ak Parti Konya Milletvekilleri ve partililer tarafından çiçeklerle karşılanan Başbakan Erdoğan, kızı Sümeyye Erdoğan, Orman ve Su İşleri Bakanı Veysel Eroğlu, yoğun güvenlik önlemleri altında toplu açılış töreninin yapılacağı Mevlana Müzesi önündeki 'Mevlana Meydanı'na Başbakanlık'a ait otobüsle gitti. - Konya 3 46 35 0
