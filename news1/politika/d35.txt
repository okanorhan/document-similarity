
2010-04-13T10:49:0Z
politika
HSYK Başkanvekili Kadir Özbek, "Pakistan'da Anayasa değişikliği üzerine yüksek yargı mensuplarının istifa ettiği" örneğini vermesinin bir istifa iması olmadığını söyledi. HSYK Başkanvekili Kadir Özbek, HSYK girişinde gazetecilerin sorularını yanıtladı. Ankara Adliyesinde dün yapılan toplantıda, Pakistan'da darbeyle iktidarı ele geçiren General Ziya Ül Hak'ın hazırlattığı Anayasa üzerine, yüksek yargı mensuplarının istifa ettiği örneğini vermesi ile ilgili, bu örneği vererek ne kastettiğinin sorulması üzerine, "Bu bir istifa iması değil. Daha önce yaşanmış, yargının ortaya koyduğu bir tepkiyi örnek gösterdim. Biz de hukuk çerçevesi içerisinde ne yapmamız gerekirse o mücadelemize devam edeceğiz, anlamında söyledim" diye konuştu. 5 65 19 0
