
2012-07-18T11:54:0Z
politika
Başbakan Recep Tayyip Erdoğan, özel uçak ANA ile saat 10.40'ta Rusya'ya gitti. Başbakan Erdoğan'ı Esenboğa Havalimanı'nda Başbakan Yardımcısı Ali Babacan, Başbakanlık Müsteşarı Efkan Ala, Ankara Emniyet Müdürü Zeki Çatalkaya ile diğer yetkililer uğurladı. Başbakan Erdoğan ile Dışişleri Bakanı Ahmet Davutoğlu, Enerji ve Tabii Kaynaklar Bakanı Taner Yıldız, Ak Parti Genel Başkan Yardımcısı Ömer Çelik ile MİT Müsteşarı Hakan Fidan da Rusya'ya gitti. Başbakan Erdoğan, Rusya Devlet Başkanı Vladimir Putin ile Suriye konusu başta olmak üzere ikili görüşmelerde bulunacak. - Ankara 1 64 20 0
