
2011-06-13T11:20:0Z
politika
Ardahan'ın Göle ilçesinde erken açılan sandıkla ilgili itirazlar sonuç vermedi. Göle'nin Yağmuroğlu köyünde 1084 nolu sandığın saat 15. 30'da açılması itirazlara sebep oldu. CHP müşahitleri tarafından tutanaklara imza atılmış olmasından dolayı, partililerin itirazı fayda vermedi. İlçe Seçim Müdür Hakimi Barış Karakuş, tutanaklara atılan imzaların tam olduğundan yapılan itirazları yersiz bularak sandık sunucunu kabul etti. 170 seçmenin oy kullandığı sandıkta 161 geçerli, 9 geçersiz oy çıkmış, AKP 83, CHP 13, bağımsız Yüksel Avşar 55, diğer partiler de 10 oy almıştı. 9 90 24 0
