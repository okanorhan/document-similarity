
2011-12-20T02:38:0Z
politika
Bursa'da karşıt görüşlü öğrenci grupları arasında çıkan kavgada 3 kişi yaralandı. Uludağ Üniversitesine (UÜ) yakın olan ve yoğunlukla üniversite öğrencilerinin yaşadığı Görükle Mahallesi'nde, gece geç saatlerde karşıt görüşlü iki grup arasında gerginlik yaşandı. Gerginliğin kısa sürede büyüyerek kavgaya dönüşmesi sonucu çıkan arbedede, 2 kişi satırla sırtından, 1 kişi de darp edilmesi sonucu yaralandı. Yaralılar, olay yerine gelen ambulansla Uludağ Üniversitesi Tıp Fakültesi Hastanesi Acil Servisine kaldırıldı. Çevik Kuvvet ekipleri, bölgede geniş güvenlik önlemi alırken, bir grup Görükle Mahallesi'nde bir süre sloganlar attıktan sonra dağıldı. 15 Aralıkta, UÜ'de karşıt görüşlü iki grup arasında çıkan kavgada 3 öğrenci yaralanmıştı. - Bursa 9 57 6 0
