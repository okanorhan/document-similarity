
2013-08-02T17:22:0Z
politika
Irak'ın Felluce kentinde cuma namazını müteakip düzenlenen Başbakan Nuri el-Maliki karşıtı gösteride Mısır'daki askeri darbe protesto edildi. Mısır'da ordunun yönetime el koymasını ve seçilmiş ilk Cumhurbaşkanı Muhammed Mursi'nin görevden uzaklaştırılmasını protesto eden göstericiler "Mısır'da askeri darbeye hayır... Hukuka evet", "Felluce halkı Mısır'daki meşru anayasayı destekliyor" yazılı dövizler taşıdı. Felluce'nin doğusundaki  uluslararası karayolu üzerinde kılınan cuma namazına binlerce kişi katıldı. Hutbede "Maliki hükümeti akledecek bir kalpten, görecek gözden ve işitecek kulaktan yoksun" diyen Cuma hatibi Şeyh Visam es-Sabri, hükümeti "zalim" olarak nitelendirdi. Irak, son aylarda sık sık protesto gösterilerine sahne oluyor. - Bağdat 5 81 12 0
