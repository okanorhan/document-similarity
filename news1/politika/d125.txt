
2013-11-12T13:04:00Z
politika
Erdoğan: "Mazlumu katledenin sıfatı Yezid'dir, mazlumun da sıfatı Hüseyin'dir" - TBMM -Başbakan ErdoğanTBMM - Başbakan Recep Tayyip Erdoğan, partisinin grup toplantısında konuştu. Başbakan Recep Tayyip Erdoğan, "Bugün de Irak'ta, Suriye'de, Yemen'de Lübnan'da kendisine hangi sıfatı takarsa taksın, mazlumu katledenin sıfatı Yezid'dir, mazlumun da sıfatı Hüseyin'dir" dedi. Başbakan Recep Tayyip Erdoğan, "Bugün dünyanın herhangi bir yerinde kendisini Müslüman olarak tanımlayan bir şahıs, üzerine bombaları sarıyor, gidiyor bir kutsal mekanda bombaları patlatıyor. Böylesine bir vahşetin, gaddarlığın bırakın Sünniliği, Şiiliği insanlıkla da alakası yoktur. İslam'la da asla ve asla yakından uzaktan alakası yoktur" dedi. 5 69 21 0
