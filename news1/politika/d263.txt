
2013-04-23T20:04:00Z
politika
Cumhurbaşkanı Abdullah Gül, "Herkesin işin içine girdiği bir süreç, bunun muhakkak başarılı olması lazım" dedi. TBMM'nin kuruluşunun 93. yılı münasebetiyle mecliste verilen resepsiyona katılan Cumhurbaşkanı Gül, "Dikkatle, sabırla çalışmak lazım. Birden bire her şeyi de toz pembe görmemek lazım ama muhakkak ki böyle büyük sorumluluktan kurtulmak için herkesin yapıcı olarak, dikkatli bir şekilde katkı vermesinin çok faydalı olacağına inanıyorum. Bu konjonktürle değil daima bu konuyla ilgili görüşlerimi paylaştım. Böyle bir aleni, bu kadar herkesin işin içine girdiği bir süreç, bunun muhakkak başarılı olması gerek" şeklinde konuştu. - Ankara 7 38 5 0
