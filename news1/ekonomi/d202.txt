
2013-03-28T19:15:0Z
ekonomi
Güney Kıbrıs Rum kesimi Dışişleri Bakanı Ioannis Kasoulides, banka işlemlerine yapılan kısıtlamaların bir ay içinde kaldırılacağını açıkladı. Kasoulides, merkez bankası değerlendirmelerine göre kısıtlamaların bir ay içinde aşama aşama kaldırılacağını söyledi. Güney Kıbrıs Rum kesiminde, 12 gündür kapalı olan bankalar bugün tekrar açılmış, hesapların boşalmasını önlemek için bankalardaki işlemlere çeşitli sınırlamalar getirilmişti. Kredi kartlarıyla ödeme yapılması ve para aktarımı gibi bankacılık işlemleri 5 bin avroyla sınırlanmış, para çekimine ise kişi başına günlük 300 avro limit koyulmuştu. Söz konusu kısıtlamalar, her gün gözden geçirilecek. - Ankara 6 61 6 0
