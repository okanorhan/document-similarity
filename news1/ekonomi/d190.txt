
2013-02-24T12:22:0Z
ekonomi
TÜRMOB Genel Başkanı Nail Sanlı, TOBB Başkanı Rifat Hisarcıklıoğlu ile bir araya gelerek iş dünyasını yakından ilgilendiren konuları masaya yatırdı. Türkiye Serbest Muhasebeci Mali Müşavirler ve Yeminli Mali Müşavirler Odaları Birliği'nden (TÜRMOB) yapılan yazılı açıklamada, TÜRMOB Başkanı Sanlı'nın, Türkiye Odalar ve Borsalar Birliği (TOBB) Başkanı Rifat Hisarcıklıoğlu'yla bir araya gelerek muhasebeci ve Mali müşavirler camiası ile iş dünyasını yakından ilgilendiren defter kapanış tasdikleri ve diğer ortak sorunlarla ilgili fikir alışverişinde bulundukları belirtildi. Açıklamada, başkanların bu sorunların çözümü için işbirliği yapacaklarını belirttiklerine de dikkat çekildi. - Ankara 4 50 15 0
