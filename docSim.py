# -*- coding: utf-8 -*-

from gensim import utils
from simserver import SessionServer
import os, io

mainPath=os.path.dirname(__file__) # ana dizin bulunuyor  
serverPath= os.path.join(mainPath,'Docsimserver') #server yolu

#similarty=[]
def dosyaokuma(dataPath): #dosya okuma fonksiyonu
    corpus=[] #index'lenecek veri
    for dirname in os.listdir(dataPath):#klasordeki veriler listeleniyor
        filesDataPath = os.path.join(dataPath,dirname)#klasorun yolu alınıyor
#        doc_num=0#index resetleniyor
        try:
            for filename in os.listdir(filesDataPath):#klasordeki veriler listeleniyor
                #dosya aciliyor                
                fileOpen = io.open(os.path.join(filesDataPath,filename), 
                               mode='r',
                               encoding='utf8')
                text =fileOpen.read().lower()#dosya okunuyor  
                fileOpen.close() #dosya kapaniyor  
                
                #corpus olusturuluyor
                corpus.append({'id': '%s_%s' % (dirname,filename), 
                               'tokens': utils.simple_preprocess(text),
                               'filename':filename})
#                doc_num +=1 #index artırılıyor         
        except:
            pass
    
    return corpus
    
def indexleme(startService,data):
    utils.upload_chunked(startService, data, chunksize=1000) # send 1k docs at a time
    startService.train(corpus=data, method='lsi')
    startService.index(data)  
    
def simScoreList(doc):
#    global service
#    mainDataPath=os.path.join(mainPath,'news1')
    service =SessionServer(serverPath) 
    benzerlik =service.find_similar(doc, min_score=0.45,max_results=50)
#    similarty.append({"doc":doc,
#                      "sim": benzerlik})
#    benzerlik=benzerlik.split("_")
#    benzerlik=os.path.join(mainDataPath,
#                           benzerlik[1],
#    benzerlik[0])

    
    return {"doc":doc,"sim": benzerlik}

if __name__ == "__main__":
    mainDataPath=os.path.join(mainPath,'news1')# haberlerin yolu
    
    if not os.path.exists(serverPath): #klasor olusturuluyor
        os.mkdir(serverPath)
    
    service = SessionServer(serverPath)#server baslatiliyor   
    
    #veri indeksle
#    data=dosyaokuma(mainDataPath)
#    indexleme(service,data)
    
    